import requests
import database


class JenkinsServer:

    def __init__(self,instance_user,instance_password,instance_url='http://127.0.0.1:8080',verify = True):
        """
        accepts jenkins instance data and connect to the instance
        :param instance_url:
        :param instance_user:
        :param instance_password:
        """
        self.session = requests.Session()
        self.session.auth =   (instance_user, instance_password)
        self.session.verify = verify
        self.base_url = instance_url


    def  get_my_job_status(self,job_url ):

        j=  self.session.get(url='%slastBuild/api/json?depth=1'%(job_url)).json()

        return j['building'],j['result']

    def  get_job(self,job_name):
        j=  self.session.get(url='%sapi/json'%( job_name)).json()

        return j


    def get_all_jobs(self):
        jobs =  self.session.get(url='%s/api/json?pretty=true' % (self.base_url )).json()

        db = database.SQLDatabase('jenkins.db')
        for j in jobs['jobs']:

            res = self.get_job(j['url'])

            if res.get('buildable'):
                building, result  = self.get_my_job_status(j['url'])
                db.save_data(j['name'],building,result)
                print('Saved job %s status to database'%(j['name']))

def main():
    server = JenkinsServer( instance_user='admin', instance_password='admin',instance_url='http://127.0.0.1:8080')
    #print(server.get_my_job_status())
    server.get_all_jobs()






if __name__ == '__main__':
    main()