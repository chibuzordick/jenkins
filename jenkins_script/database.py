import sqlite3
import datetime

class SQLDatabase():

    def __init__(self,database_name='jenkins.db'):

        self.conn  = sqlite3.connect(database_name)

    def _create_tables(self):
        c = self.conn.cursor()
        c.execute('''CREATE TABLE jobs
                     (name text, running integer, status text, time text)''')
        self.conn.commit()
        self.conn.close()

    def save_data(self,job_name,job_status,job_running):
        c = self.conn.cursor()
        c.execute(""" Insert into jobs(name,running,status,time) values (?,?,?,?)
        """,(job_name,job_status,job_running, datetime.datetime.now( ).strftime("%I:%M%p on %B %d, %Y")))

        self.conn.commit()
        self.conn.close()
    def get_saved_jobs(self):
        c = self.conn.cursor()
        c.execute("""select * from jobs 
               """)

        print(c.fetchall())
        self.conn.close()


#SQLDatabase()._create_tables()
#SQLDatabase().get_saved_jobs()